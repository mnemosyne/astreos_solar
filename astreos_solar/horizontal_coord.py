"""
_DESCRIPTION
"""



##======================================================================================================================##
##                PACKAGES                                                                                              ##
##======================================================================================================================##

import argparse
import doctest


from numpy import deg2rad, rad2deg, sin, cos, arccos #, exp, sin, cos, tan, 


from astreos_solar.config import *

##======================================================================================================================##
##                CONSTANTS                                                                                             ##
##======================================================================================================================##

print("GET solar infos from facts")




##======================================================================================================================##
##                FUNCTIONS                                                                                             ##
##======================================================================================================================##

def solar_zenith_angle(w:float, dec:float, lat:float)->float:
    """
    * Zenith angle *

    Angle of incidence of the sun beam on a horizontal surface wrt the
    normal to that surface, for a declination, a *solar* time and latitude.

    Parameters
    ----------
    w : hour angle at *solar* time
    lat : latitude (-90 to 90) in degrees
    dec : declination in radians

    Returns
    -------
    theta_z : float
        zenith angle of incidence in radians
    
    """
    
    lat = deg2rad(lat)   

    cos_theta_z = sin(dec) * sin(lat) + cos(dec) * cos(lat) * cos(w)
    res = arccos(cos_theta_z)

    return res

def solar_altitude_angle(w:float, dec:float, lat:float)->float:
    """
    * Solar altitude angle *

    Angle between the projection of the sun beam on a horizontal
    surface wrt the beam, for a declination, *solar* time and latitude.

    Parameters
    ----------
    w : hour angle at *solar* time
    lat : latitude (-90 to 90) in degrees
    dec : declination in radians

    Returns
    -------
    solar_altitude : float
        altitude angle in radians
    """
    # ~ th_z = solar_zenith_angle(date, lat)
    # ~ np.arcsin(cos(th_z))
    z = solar_zenith_angle(w=w, dec=dec, lat=lat)
    res = PI - z
    print('check')

    return res

def solar_azimuth_angle(w:float, dec:float, alt:float)->float:
    """
    * Solar azimuth angle *

    Angle between the projection of the sun beam on a horizontal surface
    wrt N-S, for a *solar*, declinason and solar altitude. Positive to the West.

    Parameters
    ----------
    w : hour angle at *solar* time
    alt : solar altitude in radians
    dec : declination in radians

    Returns
    -------
    solar_az : float
        azimuth angle in radians
    """



def solar_ned_vector(zenith, azimuth):
    """
    Function doc
    >>> 'afarie'
    """
    

##======================================================================================================================##
##                CLASSES                                                                                               ##
##======================================================================================================================##



##======================================================================================================================##
##                MAIN                                                                                                  ##
##======================================================================================================================##

if __name__ == '__main__':
    
    #+++++++++++++++++++++++++++++++#
    #    DOCTESTS                   #
    #+++++++++++++++++++++++++++++++#
    parser = argparse.ArgumentParser(description='')
    parser.add_argument('--doctests', default=False, action='store_true')
    opts = parser.parse_args()
    
    #+++++++++++++++++++++++++++++++#
    #    DOCTEST                    #
    #+++++++++++++++++++++++++++++++#
    if opts.doctests:
        test_result = doctest.testmod()
        print(test_result)
