"""
_DESCRIPTION





>>> day = datetime.datetime(2023,3,31)
>>> s_hour=12.5
>>> doy = day.timetuple().tm_yday
>>> d_angle = doy_angle(doy)
>>> w_angle = hour_angle(s_hour)
    
>>> dec = declination1(doy=doy)
>>> round(rad2deg(dec),2)
3.62
>>> dec = declination2(doy_angle=d_angle)
>>> round(rad2deg(dec),2)
3.86
>>> grelat = 45
    
>>> zenithal_angle = solar_zenith_angle(w=w_angle, lat=grelat, dec=dec)

>>> round(rad2deg(zenithal_angle),0)
42.0

"""
__author__ = "Geremy Panthou"
__license__ = "GNU GPL"


##======================================================================================================================##
##                PACKAGES                                                                                              ##
##======================================================================================================================##

import argparse
import doctest
import datetime

from numpy import rad2deg

from astreos_solar.config import *
from astreos_solar.equatorial_coord import *
from astreos_solar.horizontal_coord import *


print("place example in init from e.g. solarpy or other")


##======================================================================================================================##
##                MAIN                                                                                                  ##
##======================================================================================================================##

if __name__ == '__main__':
    
    #+++++++++++++++++++++++++++++++#
    #    DOCTESTS                   #
    #+++++++++++++++++++++++++++++++#
    parser = argparse.ArgumentParser(description='')
    parser.add_argument('--doctests', default=False, action='store_true')
    opts = parser.parse_args()
    
    #+++++++++++++++++++++++++++++++#
    #    DOCTEST                    #
    #+++++++++++++++++++++++++++++++#
    if opts.doctests:
        test_result = doctest.testmod()
        print(test_result)


    day = datetime.datetime(2023,3,31)
    s_hour=12.5
    doy = day.timetuple().tm_yday
    d_angle = doy_angle(doy)
    w_angle = hour_angle(s_hour)
    
    dec = declination1(doy=doy)
    dec = declination2(doy_angle=d_angle)
    grelat = 45
    
    zenithal_angle = solar_zenith_angle(w=w_angle, lat=grelat, dec=dec)
    
    print(rad2deg(dec))
    print(rad2deg(zenithal_angle))

