"""
The simple formulas are taken from https://perso.limsi.fr/bourdin/master/Calculs_astronomiques_simples.pdf
The complex ones comes from solarpy and https://direns.mines-paristech.fr/Sites/Thopt/fr/res/CalculsEnsoleillementSolaire.pdf
The functions names are inspired from solarpy

Simples formulae ends with 1
complex one by 2 or more


The equatorial coordinates of the Sun (declination, hour angle, ....) are given for a reference frame by the pole axis and the equator plane, the meridian of the place being taken as origin. It is in this reference frame that the daily information of the astronomical tables of the sun are given.


"""



##======================================================================================================================##
##                PACKAGES                                                                                              ##
##======================================================================================================================##

import argparse
import doctest



import numpy

from numpy import deg2rad, rad2deg, sin, cos #, arccos, exp, sin, cos, tan, 

from matplotlib import pyplot

from astreos_solar.config import *

##======================================================================================================================##
##                CONSTANTS                                                                                             ##
##======================================================================================================================##

print("GET solar infos from facts")







##======================================================================================================================##
##                FUNCTIONS                                                                                             ##
##======================================================================================================================##

#+++++++++++++++++++++++++++++++#
#    Day of the year angle      #
#+++++++++++++++++++++++++++++++#

def doy_angle(doy:int) -> float:
    """
    Day-of-the-year angle on a desired date and time.

    Parameters
    ----------
           
    doy : int
        day of the year (1 to 365 or 366)

    Returns
    -------
    B : float
        angle of the day of the year in radians
        
    >>> doy_angle(1)
    >>> doy_angle(180)
    >>> doy_angle(365)
    >>> doy_angle(366)
    
    >>> rad2deg(doy_angle(numpy.array([1,180,365,366])))
    
    """
    res = 2 * PI * (doy-1) / NDAY_IN_YEAR
    
    return res


#+++++++++++++++++++++++++++++++#
#    Hour angle                 #
#+++++++++++++++++++++++++++++++#


def hour_angle(solar_hour:float)->float:
    """
    Angular displacement of the sun east-west of the local meridian for a
    date, *solar* time and latitude.
    Note: 15 degrees per hour, morning < 0 < afternoon

    Parameters
    ----------
    solar_hour : *solar* time expressed in hour

    Returns
    -------
    hour angle : float
        local hour angle in radians
    
    >>> hour_angle(0) == -PI
    
    >>> hour_angle(12)
    0.0
    
    >>> hour_angle(24) == PI
    
    """
    res = PI * (solar_hour -12) / 12
    
    return res
    # ~ if isinstance(date, datetime):
        # ~ w = (date.hour + (date.minute / 60) - 12) * 15
        # ~ return deg2rad(w)
    # ~ else:
        # ~ raise TypeError('date must be a datetime object')



#+++++++++++++++++++++++++++++++#
#    SIMPLE FORMULAES           #
#+++++++++++++++++++++++++++++++#
# The simple formulas are taken from https://perso.limsi.fr/bourdin/master/Calculs_astronomiques_simples.pdf


def declination1(doy:int) -> float:
    """
    Angular position of the Sun at solar noon on a desired date and time.
    Must comply with -23.45º < declination < 23.45º

    Parameters
    ----------
    date : datetime object
        date of interest

    Returns
    -------
    declination : float
        declination in radians
        
        
    >>> declination1(1)
    >>> rad2deg(declination1(173))
    >>> rad2deg(declination1(81))
    >>> rad2deg(declination1(265))
    >>> rad2deg(declination1(356))
    """
    num = 2*PI * (doy+284)
    res = EARTH_OBLIQUITY_RAD * sin(num/NDAY_IN_YEAR)
    
    return res





#+++++++++++++++++++++++++++++++#
#    COMPLEX FORMULAES          #
#+++++++++++++++++++++++++++++++#







def declination2(doy_angle:int) -> float:
    """
    Angular position of the Sun at solar noon on a desired date and time.
    Must comply with -23.45º < declination < 23.45º

    Parameters
    ----------
    date : datetime object
        date of interest

    Returns
    -------
    declination : float
        declination in radians
    """
    
    res = 0.006918 - 0.399912 * cos(doy_angle) + 0.070257 * sin(doy_angle) - \
           0.006758 * cos(2 * doy_angle) + 0.000907 * sin(2 * doy_angle) - \
           0.002679 * cos(3 * doy_angle) + 0.00148 * sin(3 * doy_angle)

    return res



##======================================================================================================================##
##                CLASSES                                                                                               ##
##======================================================================================================================##



##======================================================================================================================##
##                MAIN                                                                                                  ##
##======================================================================================================================##

if __name__ == '__main__':
    
    #+++++++++++++++++++++++++++++++#
    #    DOCTESTS                   #
    #+++++++++++++++++++++++++++++++#
    parser = argparse.ArgumentParser(description='')
    parser.add_argument('--doctests', default=False, action='store_true')
    opts = parser.parse_args()
    
    #+++++++++++++++++++++++++++++++#
    #    DOCTEST                    #
    #+++++++++++++++++++++++++++++++#
    if opts.doctests:
        test_result = doctest.testmod()
        print(test_result)
        
        
        
    doys = numpy.arange(1,366)
    doy_angles = doy_angle(doys)
    
    decs1 = declination1(doys)
    decs2 = declination2(doy_angles)
    
    dif = decs2-decs1
    print(dif.min(), dif.max(), dif.mean())
    
    
    fig = pyplot.figure()
    ax = fig.gca()
    
    pyplot.plot(doys, decs1)
    pyplot.plot(doys, decs2)
    
    
    
    fig.show()
    
    
    
    
    
    
    
