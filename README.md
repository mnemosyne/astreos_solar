# astreos_solar



## Getting started




## Description




## Installation

### from gitlab

```
pip install git+https://gricad-gitlab.univ-grenoble-alpes.fr/
```

Ou

```
pip install git+ssh://git@gricad-gitlab.univ-grenoble-alpes.fr/
```

### test install

Get install directory
```
pip show 
```

Use pytest on hades_stats path
```
pytest --doctest-modules /path_wherre_pip_install/site-packages/
```

## Usage


```

```

## Ressources

Calcul trigo : https://fr.wikiversity.org/wiki/Trigonom%C3%A9trie/Relations_trigonom%C3%A9triques

Orbitre Terrestre: https://en.wikipedia.org/wiki/Earth%27s_orbit

Système de coordonées: https://en.wikipedia.org/wiki/Astronomical_coordinate_systems


Université Paris-Sud Orsay – Master PAM 2ème année – Energies renouvelables – Energie solaire thermique
Vincent Bourdin 2014
Calculs astronomiques simplifié s
https://perso.limsi.fr/bourdin/master/Calculs_astronomiques_simples.pdf



UNIVERSITE D’ANTANANARIVO
ECOLE SUPERIEURE POLYTECHNIQUE
D’ANTANANARIVO
Mention : SCIENCE ET INGENIERIE DES MATERIAUX
Parcours : Matériaux Organiques et Composites
Mémoire de fin d’études en vue de l’obtention du diplôme
Grade MASTER - Titre INGENIEUR MATERIAUX
Présenté par : RABENANDRASANA Ravaka Nandrianina
Directeur de mémoire : RAHELIARILALAO Bienvenue, Professeur Titulaire
Soutenu le Vendredi 04 Novembre 2016
PROMOTION « 2015 »
CHAUFFE-EAU SOLAIRE À THERMOSIPHON
À CAPTEUR PLAN VITRÉ :
Conception, détermination des conditions optimales d’utilisation et
expérimentation
http://biblio.univ-antananarivo.mg/pdfs/rabenandrasanaRavakaN_ESPA_MAST_16.pdf



https://direns.mines-paristech.fr/Sites/Thopt/fr/res/CalculsEnsoleillementSolaire.pdf
Estimation de l'ensoleillement reçu par un capteur solaire


solarpy package: MIT licence : Copyright (c) 2012-2019 AeroPython Team


Voir shema ici: Voir aussi : https://www.cite-sciences.fr/fileadmin/fileadmin_CSI/fichiers/vous-etes/enseignant/Documents-pedagogiques/_documents/Ressources-en-ligne/Fiche1-SystemesCoordonnees.pdf

Calcul zenith 

Partir de la definition D d'un vecteur NED
- cos (90 - lat + dec) * cos (angle horaire) = cos (lat - dec) * cos (angle horaire)



Calcul Azimuth

Partir de l'éauation sin(a) = cos(dec) sin(angle horaire) / cos(h)




## License
GNU GPL

